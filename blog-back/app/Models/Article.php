<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class Article extends Model
{
    use HasFactory;

    protected $fillable = ['titre', 'contenu', 'article_image' , 'user' , 'category'];
    //protected $primaryKey = 'id';

    public function commentaires()
    {
        return $this->hasMany(Commentaire::class , 'article');
    }
    public function likes()
    {
        return $this->hasMany(Like::class , 'article');
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user');
    }
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category');
    }

    public function scopeSearch($query, $keyword)
    {
        return $query->where('titre', 'like', '%' . $keyword . '%')
            ->orWhere('contenu', 'like', '%' . $keyword . '%');
    }

}
