<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use App\Models\Article;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'roles',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            "roles" => $this->roles,
            "username" => $this->username,
            "email" => $this->email
        ];
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
    public function commentaires()
    {
        return $this->hasMany(Commentaire::class);
    }

    public function hasLiked(Article $article)
    {
        // Vérifie si l'utilisateur a aimé l'article en comparant l'ID de l'utilisateur
        // avec la valeur du champ 'likes' dans l'article
        return $this->id === $article->likes;
    }

    public function likedArticles()
    {
        //Cherche les articles liker par l'utilisateur
        return Article::whereRaw('FIND_IN_SET(?, likes)', [$this->id])->get();
    }

    public function likes()
    {
        return $this->hasMany(Like::class , 'user');
    }

}
