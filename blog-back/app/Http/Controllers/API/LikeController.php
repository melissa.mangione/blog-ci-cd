<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    // add like in article
    public function like(Request $request, Article $article)
    {
        // keep current user auth
        $user = Auth::id();

        // check if user liked this article
        $existingLike = Like::where('user', $user)->where('article', $article->id)->first();
        if ($existingLike) {
            return response()->json(['message' => 'Vous avez déjà aimé cet article.'], 400);
        }
        // create new like
        $like = new Like();
        $like->user = $user;
        $like->article = $article->id;
        $like->save();
        // rester succes message
        return response()->json(['message' => 'Article aimé avec succès.']);
    }

    // Delete like
    public function unlike(Request $request, Article $article)
    {
        // Récupérer l'utilisateur authentifié
        $user = Auth::id();

        // Rechercher et supprimer le like
        $like = Like::where('user', $user)->where('article', $article->id)->first();
        if (!$like) {
            return response()->json(['message' => 'Vous n\'avez pas aimé cet article.'], 400);
        }
        $like->delete();
        // Retourner une réponse réussie
        return response()->json(['message' => 'Like retiré avec succès.']);
    }

    // Obtenir le nombre total de likes pour un article
    public function getLikesCount(Article $article)
    {
        // Compter les likes pour l'article donné
        $likesCount = Like::where('article', $article->id)->count();

        // Retourner le nombre total de likes
        return response()->json(['likes_count' => $likesCount]);
    }
}
