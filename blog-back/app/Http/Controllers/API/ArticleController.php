<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $articles = Article::with(['user', 'category'])
                ->orderBy('created_at', 'desc')
                ->get();
            return response()->json([
                'status' => 'Success',
                'data' => $articles
            ]);
        }catch (QueryException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la récupération des articles',
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try{
        $current = Auth::id();
        $request->validate([
            'titre' => 'required|string|max:200',
            'contenu' => 'required|string',
            'article_image' => 'nullable|mimes:png,jpg,jpeg|max:2048',
            'category' => 'required',
        ]);
        if ($request->hasFile('article_image')) {
            $filename = $this->getFilename($request);
        } else {
            $filename = Null;
        }
        $article = Article::create([
            'titre' => $request->titre,
            'contenu' => $request->contenu,
            'article_image' => $filename,
            'user' => $current,
            'category' => $request->category,
        ]);

        $article->category = $article->category()->get()[0];
        $article->user = $article->user()->get()[0];
        return response()->json([
            'status' => 'Success',
            'data' => $article,
        ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la création de l\'artiicle',
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Article $article)
    {
        try {
            $article->load(['user', 'category']);
            return response()->json($article);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Article non trouvé',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la récupération de l\'article',
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Article $article)
    {
        try {
            $current = Auth::id();
            $this->validate($request, [
                'titre' => 'required|string|max:200',
                'contenu' => 'required|string',
                'article_image' => 'nullable|mimes:png,jpg,jpeg|max:2048',
            ]);
            $filename = null;
            if ($request->hasFile('article_image')) {
                if ($article->article_image) {
                    Storage::delete("/public/uploads/articles/" . $article->article_image);
                }
                $filename = $this->getFilename($request);
                $request->article_image = $filename;
            }

            if ($request->article_image == null) {
                $request->article_image = $article->article_image;
            }

            $article->update([
                'titre' => $request->titre,
                'contenu' => $request->contenu,
                'article_image' => $request->article_image,
                'user' => $current,
                'category' => $request->category,
            ]);

            $article->category = $article->category()->get()[0];
            $article->user = $article->user()->get()[0];

            return response()->json([
                'status' => 'Mise à jour avec succès',
                'data' => $article
            ]);

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Article non trouvé',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la mise à jour de l\'article',
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Article $article)
    {
        try {
            if ($article->article_image) {
                Storage::delete("/public/uploads/articles" . $article->article_image);
            }
            $article->delete();
            return response()->json([
                'status' => 'Supprimé avec succès',
            ] , 200);

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Article non trouvé',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la suppression de l\'article',
            ], 500);
        }
    }

    /**
     * Get file name for image in article.
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function getFilename(Request $request): string
    {
        $filenameWithExt = $request->file('article_image')->getClientOriginalName();
        $filenameWithoutExt = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('article_image')->getClientOriginalExtension();
        $filename = $filenameWithoutExt . '_' . time() . '.' . $extension;
        $path = $request->file('article_image')->storeAs('public/uploads/articles', $filename);
        return $filename;
    }

    /**
     * Search article by name .
     * @param  \Illuminate\Http\Request  $request
     * @return string
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        $keyword = $request->input('q');

        if (empty($keyword)) {
            return response()->json(['error' => 'Please provide a search keyword'], 400);
        }
        $articles = Article::search($keyword)->get();
        return response()->json(['data' => $articles]);
    }
    /**
     * Get comments for a specific article.
     *
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCommentsByArticle(Article $article)
    {
        try {
            $article = Article::with('commentaires.user')->findOrFail($article->id);
            $comments = $article->commentaires;

            return response()->json([
                'status' => 'Success',
                'data' => $comments,
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Article non trouvé',
            ], 404);
        } catch (QueryException $e) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Erreur serveur interne',
            ], 500);
        }
    }

    public function getChamps(): \Illuminate\Http\JsonResponse
    {
        return response()->json([

            'titre' => 'titre',
            'contenu'=> 'contenu',
            'article_image' => 'article_image',
            'category' => 'category',
        ]);
    }

}
