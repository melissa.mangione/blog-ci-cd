<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;


class AuthController extends Controller
{

    public function __construct()
    {

    }

    /**
     * Attempt to log in a user based on the provided email and password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required|string|email',
                'password' => 'required|string',

            ],
                [
                    'email.email' => 'Ce champ attend un donnée de type email',
                    'email.required' => 'Ce champ est requis',
                    'password.required' => 'Ce champ est requis',
                    'password.string' => 'Ce champ attend un donnée de type text'
                ]
            );
            $credentials = $request->only('email', 'password');

            $token = Auth::attempt($credentials);
            if (!$token) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized',
                ], 401);
            }

            $user = Auth::user();

            return response()->json([
                'status' => 'success',
                'user' => $user,
                'token' => $token,
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Validation failed',
                'errors' => $e->errors(),
            ], 422);
        } catch (\Illuminate\Auth\AuthenticationException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Échec de l\'authentification. Vérifiez vos informations d\'identification.',
            ], 401);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Erreur lors de l\'authentification.',
            ], 500);
        }

    }

    /**
     * Attempt to register a new user with the provided username, email, and password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        try {
            // Validate the incoming request data
            $request->validate([
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8'
            ], [
                'username.required' => 'This field is required',
                'username.string' => 'This field expects data in text format',
                'email.email' => 'This field expects data in email format',
                'email.required' => 'This field is required',
                'email.unique' => 'This email is already in use',
                'password.required' => 'This field is required',
                'password.string' => 'This field expects data in text format'
            ]);

            // Set default roles if none provided
            if (empty($request->roles)) {
                $request->roles = json_encode(["ROLE_USER"]);
            } else {
                json_encode($request->roles);
            }

            // Create a new user with the provided details
            $user = User::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'roles' => $request->roles,
            ]);

            // Log in the newly registered user and generate an authentication token
            $token = Auth::login($user);

            // Return a success response with user details, token, and authorization type
            return response()->json([
                'status' => 'success',
                'message' => 'User created successfully',
                'user' => $user,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ],
                'request' => $request
            ]);

        } catch (\Exception $e) {
            // Return a generic error response for other exceptions
            return response()->json([
                'status' => 'error',
                'message' => 'Error during user creation.',
            ], 500);
        }
    }

    /**
     * Attempt to log out the currently authenticated user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            // Check if a user is currently authenticated
            if (Auth::check()) {
                // Log out the user
                Auth::logout();

                // Return a success response upon successful logout
                return response()->json([
                    'status' => 'success',
                    'message' => 'Logout successful.',
                ]);
            }

            // Return an error response if no user is currently authenticated
            return response()->json([
                'status' => 'error',
                'message' => 'No user is currently logged in.',
            ], 401);

        } catch (\Exception $e) {
            // Return a generic error response for other exceptions
            return response()->json([
                'status' => 'error',
                'message' => 'Error during logout.',
            ], 500);
        }
    }

    /**
     * Refresh the authentication token for the currently authenticated user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        // Return a success response with the refreshed token and user details
        return response()->json([
            'status' => 'success',
            'user' => Auth::user(),
            'authorization' => [
                'token' => Auth::refresh(),
                'type' => 'bearer',
            ]
        ]);
    }

    /**
     * Retrieve the details of the currently authenticated user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function currentUser()
    {
        // Return a JSON response containing the details of the currently authenticated user
        return response()->json(Auth::user());
    }

    /**public function forgotPassword(Request $request)
    {
        try {
            // Valider la requête
            $validatedData = $request->validate([
                'email' => 'required|email|exists:users,email'
            ]);

            // Générer un jeton de réinitialisation de mot de passe
            $token = Str::random(60);

            // Enregistrer le jeton dans la table password_resets
            DB::table('password_resets')->insert([
                'email' => $validatedData['email'],
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            // Envoyer un e-mail de réinitialisation de mot de passe à l'utilisateur
            Mail::to($validatedData['email'])->send(new ResetPasswordEmail($token));

            return response()->json(
                ['message' => 'Un e-mail de réinitialisation de mot de passe a été envoyé.']
                , 200);
        } catch (ValidationException $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'errors' => $e->errors()
            ], 422);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 400);
        }
    }*/

    /*public function resetPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8',
            'token' => 'required|string'
        ]);
        try {
            // find the token
            $passwordReset = DB::table('password_resets')->where('token', $request->token);
            if (!$passwordReset) {
                return response(['message' => 'token expiré ou inexistant'], 422);
            }
            // find user's email
            $user = DB::table('users')->where('email', $request->email);
            if (!$user) {
                return response(['message' => 'Utilisateur non trouvé'], 422);
            }
            // update user password
            $userUpdateResult = DB::table('users')->where('email', $request->email)->update([
                'password' => Hash::make($request->password)
            ]);

            if (!$userUpdateResult) {
                return response(['message' => 'Erreur lors de la reinitialisation du mot de passe'], 422);
            }
            // delete current token
            $deleteTokenResult = DB::table('password_resets')->where('token', $request->token)->delete();
            if (!$deleteTokenResult) {
                return response(['message' => 'Erreur lors de la suppression du token'], 422);
            }

            return response(['message' => 'Mot de passe bien reinitialisé'], 200);
        } catch (\Exception $e) {
            return response(['message' => $e->getMessage()], 400);
        }
    }*/
}
