<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Commentaire;
use App\Models\Article;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $commentaires = Commentaire::with(['user', 'article'])->get();
            return response()->json([
                'status' => 'Success',
                'data' => $commentaires,
            ]);
        } catch
        (QueryException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la récupération des commentaires',
            ], 500);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $current = Auth::id();
            $request->validate([
                'message' => 'required|string',
            ]);

            $commentaire = Commentaire::create([
                'message' => $request->message,
                'user' => $current,
                'article' => $request->article,
            ]);
            $commentaire->article = $commentaire->article()->get()[0];
            $commentaire->user = $commentaire->user()->get()[0];
            return response()->json([
                'status' => 'Success',
                'data' => $commentaire,
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la création du commentaire',
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Commentaire $commentaire
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Commentaire $commentaire)
    {
        try {
            $commentaire->load(['user', 'article']);
            return response()->json([
                'status' => 'Success',
                'data' => $commentaire,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Commentaire non trouvé',
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Commentaire $commentaire
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Commentaire $commentaire)
    {
        try {
            $current = Auth::id();
            $this->validate($request, [
                'message' => 'required|string',
            ]);
            $commentaire->update([
                'message' => $request->message,
                'user' => $current,
                'article' => $request->article
            ]);
            $commentaire->article = $commentaire->article()->get()[0];
            $commentaire->user = $commentaire->user()->get()[0];
            return response()->json([
                'status' => 'Mise à jour avec success',
                'data' => $commentaire,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Commentaire non trouvé',
            ], 404);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Commentaire $commentaire
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Commentaire $commentaire)
    {
        try {
            $commentaire->delete();
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Commentaire non trouvé',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la suppression du commentaire',
            ], 500);
        }
        return response()->json([
            'status' => 'Supprimé avec succès',
        ]);
    }

    public function getChamps(): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'message' => 'message',
            'user'=> 'user',
            'article' => 'article',
        ]);
    }


}
