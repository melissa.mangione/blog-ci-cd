<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Dotenv\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $users = User::all();
            return response()->json([
                'status' => 'Success',
                'data' => $users
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la récupération des utilisateurs',
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function show(User $user)
    {
        try {
            return response()->json([
                'status' => 'Success',
                'data' => $user,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Utilisateur non trouvée',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la récupération de l\'utilisateur',
            ], 500);
        }
    }

    /**
     * Update the user's profile (username and email).
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        try {
            $user = $request->user();
            $request->validate([
                'username' => [
                    'required',
                    'string',
                    'max:255',
                    Rule::unique('users')->ignore($user->id),
                ],
                'email' => [
                    'required',
                    'string',
                    'email',
                    'max:255',
                    Rule::unique('users')->ignore($user->id),
                ],
            ], [
                'username.required' => 'Le champ "username" est requis.',
                'username.string' => 'Le champ "username" doit être une chaîne de caractères.',
                'username.max' => 'Le champ "username" ne doit pas dépasser :max caractères.',
                'username.unique' => 'Ce nom d\'utilisateur est déjà utilisé.',

                'email.required' => 'Le champ "email" est requis.',
                'email.string' => 'Le champ "email" doit être une chaîne de caractères.',
                'email.email' => 'Le champ "email" doit être une adresse email valide.',
                'email.max' => 'Le champ "email" ne doit pas dépasser :max caractères.',
                'email.unique' => 'Cette adresse email est déjà utilisée.',
            ]);

            $user->update([
                'username' => $request->username,
                'email' => $request->email,
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Profil utilisateur mis à jour avec succès.',
                'user' => $user,
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Erreur lors de la mise à jour du profil utilisateur.',
            ], 500);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Tag $tag
     * @return JsonResponse
     */
    public function destroy(User $user)
    {
        try {
            $user->delete();
            return response()->json(['status' => 'Supprimé avec succès']);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Utilisateur non trouvé.',
            ], 404);
        } catch (QueryException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la suppression de l\'utilisateur.',
            ], 500);
        }
    }

    public function Current()
    {
        $current = Auth::user();
        return response()->json(
            ['status' => 'Success',
                'data' => $current
            ]);
    }

    /**
     * Update the password for the currently authenticated user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(Request $request)
    {
        // Validate the incoming request data
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        ]);

        // Retrieve the currently authenticated user
        $user = $request->user();

        // Check if the old password matches the user's current password
        if (Hash::check($request->old_password, auth()->user()->password)) {
            // Update the user's password with the new one
            $user->update([
                'password' => Hash::make($request->new_password)
            ]);

            // Return a success response upon successful password update
            return response()->json([
                'message' => "Password successfully updated",
            ], 200);
        } else {
            // Return an error response if the old password does not match
            return response()->json([
                'message' => "Old password does not match",
            ], 400);
        }
    }

    public function getLikedArticles()
    {
        $curent = Auth::user();
        $likedArticles = $curent->likedArticles();

        return response()->json([
            'status' => 'success',
            'liked_articles' => $likedArticles,
        ]);
    }

    public function getChamps(): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'username' => 'username',
            'email'=> 'email',
            'password' => 'password',
        ]);
    }

}
