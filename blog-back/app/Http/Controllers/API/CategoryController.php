<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $categories = DB::table('categories')->get()->toArray();
            return response()->json([
                'status' => 'Success',
                'data' => $categories,
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la récupération des catégories',
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'category_name' => 'required|string|unique:categories|max:100',
            ]);
            $category = Category::create([
                'category_name' => $request->category_name,
            ]);
            return response()->json([
                'status' => 'Success',
                'data' => $category,
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la création de la catégorie',
            ], 500);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Category $category)
    {
        try {
            return response()->json([
                'status' => 'Success',
                'data' => $category,
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Catégorie non trouvée',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la récupération de la catégorie',
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Category $category)
    {
        try {
            $this->validate($request, [
                'category_name' => 'required|string|unique:categories|max:50',
            ]);

            $category->update([
                'category_name' => $request->category_name,
            ]);

            return response()->json([
                'status' => 'Success',
                'data' => $category,
            ]);

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Catégorie non trouvée',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la mise à jour de la catégorie',
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Category $category)
    {
        try {
            $category->delete();
            return response()->json(['message' => 'Catégorie supprimée avec succès.'], 200);

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Catégorie non trouvée',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Erreur',
                'message' => 'Erreur lors de la suppression de la catégorie',
            ], 500);
        }
    }

    public function getChamps(): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'category_name' => 'category_name',
        ]);
    }
}

