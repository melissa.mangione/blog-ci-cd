<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use App\Models\Category;
use App\Models\User;
use App\Models\Article;


class ArticleControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('api/articles');
        $response->assertStatus(200);
    }
    public function test_create_article()
    {
        $user = User::create([
            'username' => 'testuserArticle',
            'email' => 'testArticle@example.com',
            'password' => bcrypt('password'),
            'roles' => json_encode(["ROLE_ADMIN"])
        ]);
        // Simule l'authentification
        $response = $this->post('/api/login', [
            'email' => 'testArticle@example.com',
            'password' => 'password',
        ]);
        $token = $response->json('token');
        // Les données pour la création de l'article
        $data = [
            'titre' => 'Test Article',
            'contenu' => 'Contenu de l\'article',
            'category' => 1,
        ];
        // Appelle la route correspondante à la fonction store
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('/api/articles' , $data);
        $response->assertStatus(200);

    }
    public function test_update_article()
    {
        // Simule l'authentification
        $response = $this->post('/api/login', [
            'email' => 'testArticle@example.com',
            'password' => 'password',
        ]);
        $token = $response->json('token');
        $article = Article::create([
            'titre' => 'Test Article Ancien',
            'contenu' => 'Contenu de l\'article',
            'category' => 1,
        ]);
        dump($response->json('user'));
        // Les données pour la création de l'article
        //creation de l'article
        dump($article);
        $Newdata = [
            'titre' => 'Test Article New',
            'contenu' => 'Contenu de l\'article',
            'category' => 1,
            'user' => 1
        ];
        // Appelle la route correspondante à la fonction store
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->patch('/api/articles/' .$article->id , $Newdata);
        $response->assertStatus(200);

    }
}
