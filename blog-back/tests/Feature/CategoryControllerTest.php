<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Category;
use App\Models\User;

class CategoryControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('api/categories');
        $response->assertStatus(200);
    }

    public function test_create()
    {
        $user = User::create([
            'username' => 'testuser',
            'email' => 'testCategory@example.com',
            'password' => bcrypt('password'),
            'roles' => json_encode(["ROLE_ADMIN"])
        ]);

        $response = $this->post('/api/login', [
            'email' => 'testCategory@example.com',
            'password' => 'password',
        ]);
        // Récupération du token de la réponse JSON
        $token = $response->json('token');
        $dataCategory = [
            'category_name' => 'test1'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('/api/categories' , $dataCategory);
        // la requête a réussi (code de statut HTTP 200)
        $response->assertStatus(201);
    }
    public function test_update_category()
    {
        // Crée une catégorie
        $category = Category::create([
            'category_name' => 'OldCategory',
        ]);

        $response = $this->post('/api/login', [
            'email' => 'testCategory@example.com',
            'password' => 'password',
        ]);
        $token = $response->json('token');
        // Les nouvelles données pour la mise à jour
        $newData = [
            'category_name' => 'NewCategory',
        ];
        // Appelle la route correspondante à la fonction update
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->put('/api/categories/'.$category->id, $newData);
        // Assure que la réponse a un statut HTTP 200
        $response->assertStatus(200);
    }

    public function test_delete_category()
    {
        // Crée une catégorie
        $category = Category::create([
            'category_name' => 'SuppCategory',
        ]);
        $response = $this->post('/api/login', [
            'email' => 'testCategory@example.com',
            'password' => 'password',
        ]);
        $response->assertStatus(200);
        $token = $response->json('token');
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->delete('/api/categories/'. $category->id);
        // Assure que la réponse a un statut HTTP 200
        $response->assertStatus(200);
    }
}
