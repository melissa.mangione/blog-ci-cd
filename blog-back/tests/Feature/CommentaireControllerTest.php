<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Article;
use App\Models\Commentaire;

class CommentaireControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('api/commentaires');
        $response->assertStatus(200);
    }
    public function test_create_commentaire()
    {
        $user = User::create([
            'username' => 'testuserCommentaire',
            'email' => 'testCommentaire@example.com',
            'password' => bcrypt('password'),
            'roles' => json_encode(["ROLE_ADMIN"])
        ]);
        // Simule l'authentification
        $response = $this->post('/api/login', [
            'email' => 'testCommentaire@example.com',
            'password' => 'password',
        ]);
        $token = $response->json('token');
        // Les données pour la création de l'article
        $data = [
            'message' => "super article",
            'article' => 1,
        ];
        // Appelle la route correspondante à la fonction store
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('/api/commentaires' , $data);
        $response->assertStatus(200);

    }
}
