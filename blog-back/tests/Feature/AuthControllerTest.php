<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // Create a new user
    public function test_register()
    {
        $response = $this->post('/api/register', [
            'email' => 'usertest@exemple.com',
            'username' => 'usertest',
            'password' => bcrypt('password'),
            'roles' => json_encode(["ROLE_USER"])
        ]);
        $response->assertStatus(200);
    }

    //login user
    public function test_login()
    {
        //create user
        $user = User::create([
            'email' => 'userlogin@exemple.com',
            'username' => 'userlogin',
            'password' => bcrypt('password'),
            'roles' => json_encode(["ROLE_USER"])
        ]);
        //login user
        $response = $this->post('/api/login', [
            'email' => 'userlogin@exemple.com',
            'password' => 'password',
        ]);
        $response->assertStatus(200);
    }


}
