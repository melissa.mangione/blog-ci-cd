<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use App\Models\User;

class UserControllerTest extends TestCase
{
    // liste des utilisateurs
    public function test_index()
    {
        $user = User::create([
            'email' => 'testg3@exemple.com',
            'username' => 'test3',
            'password' => bcrypt('password'),
            'roles' => json_encode(["ROLE_ADMIN"])
        ]);
        $response = $this->post('/api/login', [
            'email' => 'testg3@exemple.com',
            'password' => 'password',
        ]);
        // Récupération du token de la réponse JSON
        $token = $response->json('token');
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->get('/api/users');
        // la requête a réussi (code de statut HTTP 200)
        $response->assertStatus(200);

    }

    public function test_update_user_profile()
    {
        // Création manuelle de l'utilisateur
        $user = User::create([
            'username' => 'testuser',
            'email' => 'testuser@example.com',
            'password' => bcrypt('password'),
            'roles' => json_encode(["ROLE_USER"])
        ]);
        // Login de l'utilisateur et récupération du token
        $response = $this->post('/api/login', [
            'email' => 'testuser@example.com',
            'password' => 'password',
        ]);
        $response->assertStatus(200);
        // Récupération du token de la réponse JSON
        $token = $response->json('token');
        // Les données que vous souhaitez mettre à jour
        $dataToUpdate = [
            'username' => 'testuspper',
            'email' => 'testuser@example.com',
        ];
        // Appel à l'API avec le token JWT dans l'en-tête pour mettre à jour le profil utilisateur
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->patch('/api/users/' . $user->id, $dataToUpdate);
        // la requête a réussi (code de statut HTTP 200)
        $response->assertStatus(200);
    }

    // suppression d'un utilisateur
    public function test_delete()
    {
        $user = User::create([
            'email' => 'test3@exemple.com',
            'username' => 'test3',
            'password' => bcrypt('password'),
            'roles' => json_encode(["ROLE_USER"])
        ]);
        $response = $this->post('/api/login', [
            'email' => 'testuser@example.com',
            'password' => 'password',
        ]);
        $response->assertStatus(200);
        // Récupération du token de la réponse JSON
        $token = $response->json('token');
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->delete('/api/users/' . $user->id);
        $response->assertStatus(200);

    }

}
