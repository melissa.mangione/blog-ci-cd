**API Documentation**

**Authentication**

Login

    URL: /login
    Method: POST
    Description: Logs in a user.
    Request:
        Parameters:
            email (string): User's email.
            password (string): User's password.
    Response:
        Status Code: 200
        Body: User details and authentication token.

Register

    URL: /register
    Method: POST
    Description: Registers a new user.
    Request:
        Parameters:
            name (string): User's name.
            email (string): User's email.
            password (string): User's password.
    Response:
        Status Code: 201
        Body: Newly registered user details.

Logout

    URL: /logout
    Method: POST
    Description: Logs out the authenticated user.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: Logout success message.

Refresh Token

    URL: /refresh
    Method: POST
    Description: Refreshes the authentication token.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: New authentication token.

Get Current User

    URL: /current-user
    Method: GET
    Header: Autorization Bearer Token
    Description: Retrieves details of the currently authenticated user.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: Current user details.

**Article Endpoints**

**Articles**

Get All Articles

    URL: /articles
    Method: GET
    Description: Retrieves a list of all articles.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of articles.

Get Article by ID

    URL: /articles/{article}
    Method: GET
    Description: Retrieves details of a specific article by ID.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: Article details.

Create Article

    URL: /articles
    Method: POST
    Header: Autorization Bearer Token
    Description: Creates a new article.
    Request:
        Parameters:
            title (string): Article title.
            content (string): Article content.
    Response:
        Status Code: 201
        Body: Newly created article details.

Update Article

    URL: /articles/{article}
    Method: PATCH
    Header: Autorization Bearer Token
    Description: Updates an existing article by ID.
    Request:
        Parameters:
            title (string): Updated article title.
            content (string): Updated article content.
    Response:
        Status Code: 200
        Body: Updated article details.

Delete Article

    URL: /articles/{article}
    Method: DELETE
    Header: Autorization Bearer Token
    Description: Deletes an article by ID.
    Request: No additional parameters required.
    Response:
        Status Code: 204
        Body: No content.

Search Articles

    URL: /articles/search
    Method: GET
    Description: Searches for articles based on a query.
    Request:
        Parameters:
            query (string): Search query.
    Response:
        Status Code: 200
        Body: List of matching articles.

Get Comments by Article

    URL: /articles/{article}/commentaires
    Method: GET
    Description: Retrieves comments for a specific article.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of comments for the article.

Get Article Fields

    URL: /articlesChamps
    Method: GET
    Header: Autorization Bearer Token
    Description: Retrieves fields related to articles.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of article fields.


**Likes**

Like Article

    URL: /articles/{article}/like
    Method: POST
    Header: Autorization Bearer Token
    Description: Adds a like to a specific article.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: Success message.

Unlike Article

    URL: /articles/{article}/unlike
    Method: DELETE
    Header: Autorization Bearer Token
    Description: Removes a like from a specific article.
    Request: No additional parameters required.
    Response:
        Status Code: 204
        Body: No content.


**Category **


Get All Categories

    URL: /categories
    Method: GET
    Description: Retrieves a list of all categories.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of categories.

Get Category by ID

    URL: /categories/{category}
    Method: GET
    Description: Retrieves details of a specific category by ID.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: Category details.

Create Category

    URL: /categories
    Method: POST
    Header: Autorization Bearer Token
    Description: Creates a new category.
    Request:
        Parameters:
            name (string): Category name.
    Response:
        Status Code: 201
        Body: Newly created category details.

Update Category

    URL: /categories/{category}
    Method: PATCH
    Header: Autorization Bearer Token
    Description: Updates an existing category by ID.
    Request:
        Parameters:
            name (string): Updated category name.
    Response:
        Status Code: 200
        Body: Updated category details.

Delete Category

    URL: /categories/{category}
    Method: DELETE
    Header: Autorization Bearer Token
    Description: Deletes a category by ID.
    Request: No additional parameters required.
    Response:
        Status Code: 204
        Body: No content.

Get Category Fields

    URL: /categoriesChamps
    Method: GET
    Header: Autorization Bearer Token
    Description: Retrieves fields related to categories.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of category fields.


**Commentaires **

Get All Comments

    URL: /commentaires
    Method: GET
    Description: Retrieves a list of all comments.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of comments.

Get Comment by ID

    URL: /commentaires/{commentaire}
    Method: GET
    Description: Retrieves details of a specific comment by ID.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: Comment details.

Create Comment

    URL: /commentaires
    Method: POST
    Header: Autorization Bearer Token
    Description: Creates a new comment.
    Request:
        Parameters:
            content (string): Comment content.
            article_id (int): ID of the associated article.
    Response:
        Status Code: 201
        Body: Newly created comment details.

Update Comment

    URL: /commentaires/{commentaire}
    Method: PATCH
    Header: Autorization Bearer Token
    Description: Updates an existing comment by ID.
    Request:
        Parameters:
            content (string): Updated comment content.
    Response:
        Status Code: 200
        Body: Updated comment details.

Delete Comment

    URL: /commentaires/{commentaire}
    Method: DELETE
    Header: Autorization Bearer Token
    Description: Deletes a comment by ID.
    Request: No additional parameters required.
    Response:
        Status Code: 204
        Body: No content.

Get Comment Fields

    URL: /commentairesChamps
    Method: GET
    Description: Retrieves fields related to comments.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of comment fields.

**Users**

Get Current User

    URL: /users/current-user
    Method: GET
    Header: Autorization Bearer Token
    Description: Retrieves details of the currently authenticated user.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: Current user details.

Get All Users

    URL: /users
    Method: GET
    Header: Autorization Bearer Token
    Description: Retrieves a list of all users.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of users.

Get User by ID

    URL: /users/{user}
    Method: GET
    Header: Autorization Bearer Token
    Description: Retrieves details of a specific user by ID.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: User details.

Update User

    URL: /users/{user}
    Method: PATCH
    Header: Autorization Bearer Token
    Description: Updates an existing user by ID.
    Request:
        Parameters:
            name (string): Updated user name.
            email (string): Updated user email.
    Response:
        Status Code: 200
        Body: Updated user details.

Change Password

    URL: /users/change-password
    Method: POST
    Header: Autorization Bearer Token
    Description: Changes the password for the authenticated user.
    Request:
        Parameters:
            old_password (string): Current password.
            new_password (string): New password.
    Response:
        Status Code: 200
        Body: Success message.

Delete User

    URL: /users/{user}
    Method: DELETE
    Header: Autorization Bearer Token
    Description: Deletes a user by ID.
    Request: No additional parameters required.
    Response:
        Status Code: 204
        Body: No content.

Get Liked Articles by User

    URL: /users/{user}/liked-articles
    Method: GET
    Header: Autorization Bearer Token
    Description: Retrieves articles liked by a specific user.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of liked articles.

Get User Fields

    URL: /usersChamps
    Method: GET
    Header: Autorization Bearer Token
    Description: Retrieves fields related to users.
    Request: No additional parameters required.
    Response:
        Status Code: 200
        Body: List of user fields.        

