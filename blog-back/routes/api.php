<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ArticleController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\CommentaireController;
use App\Http\Controllers\API\LikeController;
use App\Http\Controllers\API\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');
    Route::get('current-user', 'currentUser');
});

Route::controller(ArticleController::class)->group(function () {
    Route::get('articles', 'index');
    Route::get('articles/{article}', 'show');
    Route::post('articles', 'store')->middleware('auth:api');
    Route::patch('articles/{article}', 'update')->middleware('auth:api');
    Route::delete('articles/{article}', 'destroy')->middleware('auth:api');
    Route::get('articles/search', 'search');
    Route::get('articles/{article}/commentaires', 'getCommentsByArticle');
    Route::get('articlesChamps', 'getChamps')->middleware('auth:api');
});

Route::controller(LikeController::class)->group(function () {
    Route::post('articles/{article}/like', 'like')->middleware('auth:api');;
    Route::delete('articles/{article}/unlike', 'unlike')->middleware('auth:api');
    Route::get('articles/{article}/likeCount', 'getLikesCount');
});

Route::controller(CategoryController::class)->group(function () {
    Route::get('categories', 'index');
    Route::get('categories/{category}', 'show');
    Route::post('categories', 'store')->middleware('auth:api');
    Route::patch('categories/{category}', 'update')->middleware('auth:api');
    Route::delete('categories/{category}', 'destroy')->middleware('auth:api');
    Route::get('categoriesChamps', 'getChamps')->middleware('auth:api');
});
Route::controller(CommentaireController::class)->group(function () {
    Route::get('commentaires', 'index');
    Route::get('commentaires/{commentaire}', 'show');
    Route::post('commentaires', 'store')->middleware('auth:api');
    Route::patch('commentaires/{commentaire}', 'update')->middleware('auth:api');
    Route::delete('commentaires/{commentaire}', 'destroy')->middleware('auth:api');
    Route::get('commentairesChamps', 'getChamps')->middleware('auth:api');
});
Route::controller(UserController::class)->group(function () {
    Route::get('users/current-user', 'Current');
    Route::get('users', 'index')->middleware('auth:api');
    Route::get('users/{user}', 'show')->middleware('auth:api');
    Route::patch('users/{user}', 'update')->middleware('auth:api');
    Route::post('users/change-password', 'updatePassword')->middleware('auth:api');
    Route::delete('users/{user}', 'destroy')->middleware('auth:api');
    Route::get('users/{user}/liked-articles', 'getLikedArticles')->middleware('auth:api');
    Route::get('usersChamps', 'getChamps')->middleware('auth:api');
});
