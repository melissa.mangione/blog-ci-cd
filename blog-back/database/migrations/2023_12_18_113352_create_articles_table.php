<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Article;
use Database\Factories\ArticleFactory;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('titre' , 255);
            $table->text('contenu');
            $table->string('article_image')->nullable();
            $table->timestamps();
            $table->foreignId('user');
            // Clé étrangère vers la table des categories
            $table->foreignId('category');
        });
        Article::factory()->count(2)->create();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
};
