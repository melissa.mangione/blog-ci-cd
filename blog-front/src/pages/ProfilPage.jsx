import React, { useEffect, useState } from "react";
import { errorMessage } from "../components/Toast";
import axios from "axios";
import { API_URL } from "../App";
import Form from "../components/Form";

const ProfilPage = ({ currentUser, setShowToast }) => {
  const [username, setUsername] = useState(currentUser?.username);
  const [email, setEmail] = useState(currentUser?.email);
  const [isValidate, setIsValidate] = useState(false);
  const token = localStorage.getItem("token");

  useEffect(() => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    let isValideChamps = [];
    if (username !== currentUser?.username || email !== currentUser?.email) {
      isValideChamps.push(true);
    }
    if (username !== "") {
      isValideChamps.push(true);
    }
    if (email.match(emailRegex)) {
      isValideChamps.push(true);
    }

    setIsValidate(isValideChamps.length > 2);
  }, [email, username]);

  useEffect(() => {
    if (!currentUser) {
      const fetchCurrentUser = async () => {
        try {
          if (token) {
            const apiUrl = `${API_URL}/api/users/current-user`;
            const config = {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            };

            const response = await axios.get(apiUrl, config);
            setUsername(response.data.data.username);
            setEmail(response.data.data.email);
          }
        } catch (error) {
          console.error(error);
        }
      };

      fetchCurrentUser();
    }
  }, []);

  const handleUpdate = async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      await axios.patch(
        API_URL + "/api/users/" + currentUser.id,
        {
          email,
          username,
        },
        config
      );
      location.reload();
    } catch (error) {
      console.log(error);
      errorMessage(setShowToast);
    }
  };

  return (
    <>
      <Form
        validate={isValidate}
        title={"Votre Profil"}
        action={handleUpdate}
        button={"Modifier le profil"}
      >
        <div className="w-full flex flex-col">
          <label htmlFor="email" className="mb-2 text-sm">
            Email:
          </label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className="p-2 border border-gray-400 rounded focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="w-full flex flex-col">
          <label htmlFor="usename" className="mb-2 text-sm">
            Username
          </label>
          <input
            type="text"
            id="username"
            placeholder="Entrez votre pseudo"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            className="p-2 border border-gray-400 rounded focus:outline-none focus:border-blue-500"
          />
        </div>
      </Form>
    </>
  );
};

export default ProfilPage;
