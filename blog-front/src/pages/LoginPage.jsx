import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { errorMessage } from "../components/Toast";
import { API_URL } from "../App";
import Form from "../components/Form";

const LoginPage = ({ setShowToast, setCurrentUser }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const [isValidate, setIsValidate] = useState(false);

  useEffect(() => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    let isValideChamps = [];

    if (email.match(emailRegex)) {
      isValideChamps.push(true);
    }
    if (password.length > 6) {
      isValideChamps.push(true);
    }

    setIsValidate(isValideChamps.length > 1);
  }, [email, password]);

  const handleLogin = async () => {
    try {
      // Send login credentials to the server
      const response = await axios.post(API_URL + "/api/login", {
        email,
        password,
      });

      // Check the response status
      if (response.status === 200) {
        const token = response.data.token;
        localStorage.setItem("token", token);
        setCurrentUser(response.data.user);

        // Successful login, navigate to home page
        navigate("/");
      } else {
        // Handle unexpected server response
        console.error("Unexpected server response:", response);
        errorMessage(setShowToast, "Unexpected server response");
      }
    } catch (error) {
      // Handle network error or server error
      console.error("Login failed:", error);
      errorMessage(setShowToast, "Login failed");
    }
  };

  return (
    <>
      <Form
        title="Se connecter"
        action={handleLogin}
        button={"Se connecter"}
        validate={isValidate}
      >
        <div className="w-full flex flex-col">
          <label htmlFor="email" className="mb-2 text-sm">
            Email:
          </label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className="p-2 border border-gray-400 rounded focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="w-full flex flex-col">
          <label htmlFor="password" className="mb-2 text-sm">
            Mot de passe:
          </label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className="p-2 border border-gray-400 rounded focus:outline-none focus:border-blue-500"
          />
        </div>
      </Form>
    </>
  );
};

export default LoginPage;
