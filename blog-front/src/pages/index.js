import Homepage from "./Homepage";
import AboutPage from "./AboutPage.jsx";
import LoginPage from "./LoginPage";
import SignupPage from "./SignupPage";
import ProfilPage from "./ProfilPage";
import DashboardPage from "./DashboardPage";

export {
  Homepage,
  AboutPage,
  LoginPage,
  SignupPage,
  ProfilPage,
  DashboardPage,
};
