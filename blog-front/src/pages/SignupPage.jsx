import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { errorMessage } from "../components/Toast";
import { API_URL } from "../App";
import Form from "../components/Form";

const SignupPage = ({ setShowToast, setCurrentUser }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");

  const navigate = useNavigate();

  const [isValidate, setIsValidate] = useState(false);

  useEffect(() => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    let isValideChamps = [];
    if (username !== "") {
      isValideChamps.push(true);
    }
    if (email.match(emailRegex)) {
      isValideChamps.push(true);
    }
    if (password.length > 6) {
      isValideChamps.push(true);
    }

    setIsValidate(isValideChamps.length > 2);
  }, [email, password, username]);

  const handleSignup = async () => {
    try {
      // Send user data to the server
      const response = await axios.post(API_URL + "/api/register", {
        email,
        username,
        password,
      });

      // Check the response status
      if (response.status === 200) {
        const token = response.data.authorization.token;
        localStorage.setItem("token", token);
        setCurrentUser(response.data.user);

        // Successful registration, navigate to home page
        navigate("/");
      } else {
        // Handle unexpected server response
        console.error("Unexpected server response:", response);
        errorMessage(setShowToast, "Unexpected server response");
      }
    } catch (error) {
      // Handle network error or server error
      console.error("Registration failed:", error);
      errorMessage(setShowToast, "Registration failed");
    }
  };

  return (
    <>
      <Form
        title="S'inscrire"
        action={handleSignup}
        button={"S'inscrire"}
        validate={isValidate}
      >
        <div className="w-full flex flex-col">
          <label htmlFor="email" className="mb-2 text-sm">
            Email:
          </label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className="p-2 border border-gray-400 rounded focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="w-full flex flex-col">
          <label htmlFor="username" className="mb-2 text-sm">
            Username:
          </label>
          <input
            type="text"
            id="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            className="p-2 border border-gray-400 rounded focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="w-full flex flex-col">
          <label htmlFor="password" className="mb-2 text-sm">
            Mot de passe:
          </label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className="p-2 border border-gray-400 rounded focus:outline-none focus:border-blue-500"
          />
        </div>
      </Form>
    </>
  );
};

export default SignupPage;
