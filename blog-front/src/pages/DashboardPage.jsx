import React, { useEffect, useState } from "react";
import TableDashboard from "../components/dashboard/TableDashboard";
import useFetch from "../hooks/useFetch";
import SelectDashboard from "../components/dashboard/SelectDashboard";
import CreateDashboard from "../components/dashboard/CreateDashboard";
import ModifyDashboard from "../components/dashboard/ModifyDashboard";

const DashboardPage = ({ handleCreateOrModifyOrDelete }) => {
  const [table, setTable] = useState("articles");
  const [showCreateForm, setShowCreateForm] = useState(false);
  const [showModifyForm, setShowModifyForm] = useState(false);
  const [dataToModify, setDataToModify] = useState([]);
  const [dataToShow, setDataToShow] = useState([]);
  const [wait, setWait] = useState(false);

  let { loading, data, error } = useFetch(table);

  useEffect(() => {
    setWait(true);
    if (data) {
      setDataToShow(data?.data);
      setWait(false);
    }
  }, [data]);

  const objectIdToModify = (id) => {
    setShowModifyForm(true);
    setDataToModify(dataToShow.filter((d) => d.id === id));
  };

  if (wait) return <>Toto</>;
  return (
    <div>
      {!error ? (
        !loading ? (
          <div className="dashboard-container flex flex-col items-center space-y-4">
            {showCreateForm && (
              <CreateDashboard
                setDataToShow={setDataToShow}
                handleCreateOrModifyOrDelete={handleCreateOrModifyOrDelete}
                table={table}
                setShowCreateForm={setShowCreateForm}
              />
            )}
            {showModifyForm && (
              <ModifyDashboard
                handleCreateOrModifyOrDelete={handleCreateOrModifyOrDelete}
                dataToModify={dataToModify}
                setDataToShow={setDataToShow}
                table={table}
                setShowModifyForm={setShowModifyForm}
              />
            )}
            {!showCreateForm && !showModifyForm && (
              <>
                <h1 className="text-4xl font-bold my-5 ">Liste des {table}</h1>
                <SelectDashboard table={table} setTable={setTable} />
                <button
                  onClick={() => setShowCreateForm(true)}
                  className="bg-green-500 text-white px-4 py-2 rounded mr-2  border-none hover:bg-green-600 hover:text-[#E5E5E5]"
                >
                  Créer
                </button>
                <TableDashboard
                  handleCreateOrModifyOrDelete={handleCreateOrModifyOrDelete}
                  data={dataToShow}
                  setDataToShow={setDataToShow}
                  table={table}
                  objectIdToModify={objectIdToModify}
                />
              </>
            )}
          </div>
        ) : (
          <p>Loading...</p>
        )
      ) : (
        <p>Error</p>
      )}
    </div>
  );
};

export default DashboardPage;
