import React from "react";
import { Blogs } from "../components";

const Homepage = ({ blogs }) => {
  // Renommez le tableau de blogs en minuscules
  const blogToShow = blogs.slice(0, 3);
  return (
    <div>
      <Blogs blogs={blogToShow} />
    </div>
  );
};

export default Homepage;
