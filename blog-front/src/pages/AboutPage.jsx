import React from "react";

const AboutPage = () => {
  return (
    <div>
      <div className="w-full py-8 px-5">
        <div className="max-w-[1240px] mx-auto border-gray-600 py-8">
          <section className="about-section bg-light px-5">
            <div className="container">
              <h2 className="mb-4 text-center text-4xl pb-8">About Us</h2>
              <p className="lead">
                Welcome to our world of innovation and collaboration! We're
                passionate about exploring the ins and outs of DevOps processes,
                continuous deployment, and continuous integration. Our dedicated
                blog is a treasure trove of knowledge where we share insights
                that revolutionize the way software is developed, tested, and
                delivered.
              </p>
              <p className="lead">
                Join us on a journey where we unravel the mysteries of DevOps,
                delve into the intricacies of continuous deployment, and embrace
                the power of continuous integration. Our mission is to empower
                developers, engineers, and tech enthusiasts with the latest
                trends and best practices in the ever-evolving landscape of
                software development.
              </p>

              <h2 className="mb-4 text-center text-4xl py-8">Team</h2>
              {/* Responsive Card Section with Hover Animation */}
              <div className="flex flex-col md:flex-row justify-center space-y-4 md:space-y-0 md:space-x-4">
                {/* Card 1 */}
                <div className="max-w-md md:max-w-lg bg-white shadow-md rounded p-4 transform hover:scale-105 transition-transform duration-300">
                  <img
                    src="../src/assets/img/Fichier 3.png"
                    alt="Melissa"
                    className="rounded-full mb-4 w-32 h-32 mx-auto"
                  />
                  <p className="text-center text-lg font-semibold">Melissa</p>
                </div>

                {/* Card 2 */}
                <div className="max-w-md md:max-w-lg bg-white shadow-md rounded p-4 transform hover:scale-105 transition-transform duration-300">
                  <img
                    src="../src/assets/img/Fichier 1.png"
                    alt="Clément"
                    className="rounded-full mb-4 w-32 h-32 mx-auto"
                  />
                  <p className="text-center text-lg font-semibold">Clément</p>
                </div>

                {/* Card 3 */}
                <div className="max-w-md md:max-w-lg bg-white shadow-md rounded p-4 transform hover:scale-105 transition-transform duration-300">
                  <img
                    src="../src/assets/img/Fichier 2.png"
                    alt="Lucas"
                    className="rounded-full mb-4 w-32 h-32 mx-auto"
                  />
                  <p className="text-center text-lg font-semibold">Lucas</p>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

export default AboutPage;
