import React from "react";
import { Blogs } from "../components";

const BlogPage = ({ blogs }) => {
  return (
    <div>
      <div className="w-full py-8 px-5">
        <div className="max-w-[1240px] mx-auto border-gray-600 py-8">
          <section className="about-section bg-light px-5">
            <div className="container">
              <h1 className="mb-4 text-center text-4xl">Nos blog</h1>
              <div className="flex justify-center">
                <input
                  type="text"
                  placeholder="Search Article by keyword..."
                  className="border-2 border-gray-300 p-2 w-1/4 rounded-md mr-2"
                />
                <button className="text-white p-2">Search</button>
              </div>
              <Blogs blogs={blogs} />
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

export default BlogPage;
