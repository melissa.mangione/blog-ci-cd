import {
  Homepage,
  LoginPage,
  SignupPage,
  ProfilPage,
  DashboardPage,
} from "./pages";
import { Routes, Route, Navigate } from "react-router-dom";
import useFetch from "./hooks/useFetch";
import Toast, { errorMessage } from "./components/Toast";
import { useEffect, useState } from "react";
import AboutPage from "./pages/AboutPage.jsx";
import { BlogContent, Footer, Navbar } from "./components/index.js";
import BlogPage from "./pages/BlogPage.jsx";
import PrivateRoute from "./components/PrivateRoute.jsx";
import axios from "axios";

export const API_URL = "http://127.0.0.1:8000";

export default function App() {
  const [showToast, setShowToast] = useState(false);
  const [mainLoading, setMainLoading] = useState(false);
  const [currentUser, setCurrentUser] = useState(null);
  let { loading, data, error } = useFetch("articles");

  let {
    data: currentUserData,
    loading: currentUserLoading,
    error: currentUserError,
  } = useFetch("users/current-user");

  const handleCreateOrModifyOrDelete = async ({
    method,
    table,
    data,
    goBack,
    id,
  }) => {
    try {
      setMainLoading(true);
      const token = localStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const response =
        method === "patch"
          ? await axios[method](
              API_URL + "/api/" + table + "/" + id,
              data,
              config
            )
          : method === "delete"
          ? await axios[method](API_URL + "/api/" + table + "/" + id, config)
          : await axios[method](API_URL + "/api/" + table, data, config);

      if (goBack) goBack();
      return response;
    } catch (error) {
      console.log(error);
      setShowToast(true);
      setTimeout(() => {
        setShowToast(false);
      }, 5000);
    } finally {
      setMainLoading(false);
    }
  };

  useEffect(() => {
    if (currentUserError) {
      errorMessage(setShowToast);
    }
    setCurrentUser(currentUserData?.data);
  }, [currentUserError, currentUserData]);

  if (loading || currentUserLoading) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;

  return (
    <div>
      {showToast && <Toast />}
      <Navbar currentUser={currentUser} />
      <Routes>
        <Route path="/" element={<Homepage blogs={data.data} />} />
        <Route path={"/about"} element={<AboutPage />} />
        <Route path={"/blog"} element={<BlogPage blogs={data.data} />} />
        <Route
          path="/blog/:id"
          element={
            <BlogContent
              handleCreateOrModifyOrDelete={handleCreateOrModifyOrDelete}
              blogs={data.data}
              currentUser={currentUser}
            />
          }
        />
        <Route
          path="/login"
          element={
            <LoginPage
              setShowToast={setShowToast}
              setCurrentUser={setCurrentUser}
            />
          }
        />
        <Route
          path="/signup"
          element={
            <SignupPage
              setShowToast={setShowToast}
              setCurrentUser={setCurrentUser}
            />
          }
        />

        <Route
          path="/profil"
          element={
            <PrivateRoute isAllowed={!!currentUser}>
              <ProfilPage
                currentUser={currentUser}
                setShowToast={setShowToast}
              />
            </PrivateRoute>
          }
        />
        <Route
          path="/back-office"
          element={
            <PrivateRoute
              isAllowed={
                !!currentUser && currentUser.roles === '["ROLE_ADMIN"]'
              }
            >
              <DashboardPage
                handleCreateOrModifyOrDelete={handleCreateOrModifyOrDelete}
              />
            </PrivateRoute>
          }
        />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
      <Footer />
    </div>
  );
}
