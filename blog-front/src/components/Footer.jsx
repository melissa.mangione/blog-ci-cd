import React from 'react';
import {FaDiscord} from 'react-icons/fa';


const Footer = () => {
  return (
    <div className='w-full bg-[#02044A] text-gray-300 py-8 px-5'>
        <div className='max-w-[1240px] mx-auto grid grid-cols- md:grid-cols-4 border-b-2 border-gray-600 py-8'>
              <div className='flex flex-col mb-5 px-5'>
                    <h1 className='text-2xl font-bold mb-3'>About us</h1>
                    <p className='text-sm'>Explore with us the ins and outs of DevOps processes, continuous deployment and continuous integration in our dedicated blog. Learn how these practices are revolutionizing the way software is developed, tested, and delivered.</p>
              </div>
                <div className='flex flex-col mb-5 px-5'>
                    <h1 className='text-2xl font-bold mb-3'>Quick Links</h1>
                    <ul className='text-sm'>
                        <li><a href='/'>Home</a></li>
                        <li><a href='/about'>About</a></li>
                        <li><a href='/blog'>Blogs</a></li>
                    </ul>
                </div>
                <div className='flex flex-col mb-5 px-5'>
                    <h1 className='text-2xl font-bold mb-3'>Categories</h1>
                    <ul className='text-sm'>
                        <li><a href='#'>Web Development</a></li>
                        <li><a href='#'>Tech Gadgets</a></li>
                        <li><a href='#'>Artificial Intelligence</a></li>
                        <li><a href='#'>Data Science</a></li>
                    </ul>
                </div>
                <div className='flex flex-col mb-5 px-5'>
                    <h1 className='text-2xl font-bold mb-3'>Follow Us</h1>
                    <div className='flex space-x-4'>
                        <a href='https://discord.gg/3fJ8p5E8GP' target={"_blank"}><FaDiscord className='text-2xl cursor-pointer' /></a>
                    </div>
                </div>

        </div>

        <div className='flex flex-col max-w-[1240px] px-2 py-4 m-auto justify-between flex-row text-center text-gray-500 items-center'>
            <p>Clément -Lucas - Mélissa . All rights reserved.</p>
        </div>

    </div>
  )
}

export default Footer