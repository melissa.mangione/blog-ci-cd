import React from "react";
import { TABLES, capitalizeFirstLetter } from "../../utils";

const SelectDashboard = ({ table, setTable }) => {
  
  return (
    <>
      <div className="relative inline-block text-left">
        <select
          className="block appearance-none w-full bg-white border border-gray-300 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline-blue focus:border-blue-300"
          id="table"
          name="table"
          value={table}
          onChange={(e) => setTable(e.target.value)}
        >
          {TABLES.map((p) => {
            return (
              <option key={p} value={p}>
                {capitalizeFirstLetter(p)}
              </option>
            );
          })}
        </select>
        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
          <svg
            className="fill-current h-4 w-4"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path d="M10 12a2 2 0 100-4 2 2 0 000 4z" />
            <path
              fillRule="evenodd"
              d="M19.707 5.293a1 1 0 00-1.414-1.414L10 13.586l-7.293-7.293a1 1 0 10-1.414 1.414L10 16.414l9.707-9.707a1 1 0 001.414 0z"
            />
          </svg>
        </div>
      </div>
    </>
  );
};

export default SelectDashboard;
