import React, { useEffect, useState } from "react";
import {
  capitalizeFirstLetter,
  createObjectForTable,
  removeLastS,
  typeInput,
  validateFormDatas,
} from "../../utils";
import Form from "../Form";

const ModifyDashboard = ({
  dataToModify,
  setDataToShow,
  table,
  setShowModifyForm,
  handleCreateOrModifyOrDelete,
}) => {
  const { structureObj, loading, error } = createObjectForTable(table);
  const [isValidate, setIsValidate] = useState(false);
  const [dataModify, setDataModify] = useState(() => {
    const modifiedRest = {};
    Object.keys(dataToModify[0]).forEach((key) => {
      modifiedRest[key] =
        typeof dataToModify[0][key] === "object"
          ? dataToModify[0][key]?.id ?? null
          : dataToModify[0][key];
    });
    return modifiedRest;
  });

  useEffect(() => {
    if (!loading) {
      validateFormDatas(table, dataModify, setIsValidate);
    }
  }, [dataModify]);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;
  return (
    <Form
      title={`Modifier un ${removeLastS(table)}`}
      button={`Modifier`}
      action={async () => {
        const res = await handleCreateOrModifyOrDelete({
          method: "patch",
          table,
          data: dataModify,
          goBack: () => setShowModifyForm(false),
          id: dataToModify[0].id,
        });
        if (res) {
          setDataToShow((prev) => {
            const index = prev.findIndex(
              (data) => data.id === dataToModify[0].id
            );

            if (index !== -1) {
              // Créer un nouveau tableau en remplaçant l'élément modifié
              const updatedData = [...prev];
              updatedData[index] = res.data.data;

              return updatedData;
            }

            return prev;
          });
        }
      }}
      validate={isValidate}
    >
      {Object.keys(structureObj).map((label) => {
        return (
          <div key={label} className="w-full flex flex-col">
            <label htmlFor={label} className="mb-2 text-sm">
              {capitalizeFirstLetter(label)}
            </label>
            <input
              value={dataModify[label]}
              onChange={(e) =>
                setDataModify((prev) => ({
                  ...prev,
                  [label]: e.target.value,
                }))
              }
              type={typeInput(label)}
              id={label}
              className="p-2 border border-gray-400 rounded focus:outline-none focus:border-blue-500"
            />
          </div>
        );
      })}
      <button
        onClick={() => setShowModifyForm(false)}
        className="btn p-2 mt-5 w-full bg-red-500"
      >
        Annuler
      </button>
    </Form>
  );
};

export default ModifyDashboard;
