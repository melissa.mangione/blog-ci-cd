import React, { useEffect, useState } from "react";
import {
  capitalizeFirstLetter,
  createObjectForTable,
  removeLastS,
  typeInput,
  validateFormDatas,
} from "../../utils";
import Form from "../Form";

const CreateDashboard = ({
  table,
  setShowCreateForm,
  handleCreateOrModifyOrDelete,
  setDataToShow,
}) => {
  const {
    structureObj,
    loading: loadingCreateObject,
    error,
  } = createObjectForTable(table);
  const [dataToCreate, setDataToCreate] = useState(structureObj);
  const [isValidate, setIsValidate] = useState(false);

  useEffect(() => {
    setDataToCreate(structureObj);
  }, [structureObj]);

  useEffect(() => {
    if (!loadingCreateObject) {
      validateFormDatas(table, dataToCreate, setIsValidate);
    }
  }, [dataToCreate]);

  if (loadingCreateObject) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;

  return (
    <Form
      title={`Créer un ${removeLastS(table)}`}
      button={`Créer`}
      action={async () => {
        const res = await handleCreateOrModifyOrDelete({
          method: "post",
          table,
          data: dataToCreate,
          goBack: () => setShowCreateForm(false),
        });
        if (res) {
          setDataToShow((prev) => [...prev, res.data.data]);
        }
      }}
      validate={isValidate}
    >
      {Object.keys(structureObj).map((label) => {
        return (
          <div key={label} className="w-full flex flex-col">
            <label htmlFor={label} className="mb-2 text-sm">
              {capitalizeFirstLetter(label)}
            </label>
            <input
              value={dataToCreate[label]}
              onChange={(e) =>
                setDataToCreate((prev) => ({
                  ...prev,
                  [label]: e.target.value,
                }))
              }
              type={typeInput(label)}
              id={label}
              className="p-2 border border-gray-400 rounded focus:outline-none focus:border-blue-500"
            />
          </div>
        );
      })}
      <button
        onClick={() => setShowCreateForm(false)}
        className="btn p-2 mt-5 w-full bg-red-500"
      >
        Annuler
      </button>
    </Form>
  );
};

export default CreateDashboard;
