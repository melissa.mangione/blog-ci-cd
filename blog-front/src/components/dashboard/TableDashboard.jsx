import React, { useEffect, useState } from "react";

const TableDashboard = ({
  data,
  setDataToShow,
  table,
  objectIdToModify,
  handleCreateOrModifyOrDelete,
}) => {
  const [keys, setKeys] = useState([]);

  useEffect(() => {
    if (data) {
      setKeys(Object.keys(data[0]));
    }
  }, [data]);

  return (
    <div className="container">
      <div className="my-10 mx-5">
        <h1 className="text-2xl font-bold mb-4">Table {table}</h1>
        <div className="overflow-x-auto">
          <table className="w-full table-auto text-center sm:w-full md:w-full lg:w-full xl:w-full">
            <thead className="bg-gray-800 text-white">
              <tr>
                {keys?.map((k) => (
                  <th key={k} className="p-2 sm:p-2 md:p-2 lg:p-2 xl:p-2">
                    {k}
                  </th>
                ))}
                <th className="p-2 sm:p-2 md:p-2 lg:p-2 xl:p-2">Actions</th>
              </tr>
            </thead>
            <tbody>
              {data.map((d, index) => (
                <tr
                  key={d.id}
                  className={` ${
                    index % 2 === 0 ? "bg-gray-100" : "bg-white"
                  } sm:block md:table-row lg:table-row xl:table-row`}
                >
                  {keys.map((k) => (
                    <td key={k} className="p-2 sm:p-2 md:p-2 lg:p-2 xl:p-2">
                      {typeof d[k] === "object" ? d[k]?.id ?? "null" : d[k]}
                    </td>
                  ))}
                  <td className="p-2 sm:p-2 md:p-2 lg:p-2 xl:p-2">
                    {table !== "users" && (
                      <button
                        onClick={() => objectIdToModify(d.id)}
                        className="btn bg-yellow-500 text-white hover:bg-yellow-600 hover:text-[#E5E5E5] border-none py-1 px-3 rounded-md m-1 sm:block md:inline lg:inline xl:inline"
                      >
                        Modifier
                      </button>
                    )}
                    <button
                      onClick={async () => {
                        const res = await handleCreateOrModifyOrDelete({
                          method: "delete",
                          table,
                          id: d.id,
                        });
                        if (res) {
                          setDataToShow(
                            data.filter((data) => data.id !== d.id)
                          );
                        }
                      }}
                      className="btn bg-red-500 text-white hover:bg-red-600 hover:text-[#E5E5E5] border-none py-1 px-3 rounded-md m-1 sm:block md:inline lg:inline xl:inline"
                    >
                      Supprimer
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default TableDashboard;
