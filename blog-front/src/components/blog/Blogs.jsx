import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { API_URL } from "../../App";
import axios from "axios";
import { errorMessage } from "../Toast";

const Blogs = ({ blogs }) => {
  const [likedBlogs, setLikedBlogs] = useState([]);
  const token = localStorage.getItem("token");
  const defaultImage =
    "https://mir-s3-cdn-cf.behance.net/project_modules/fs/876c22100707927.5f0ec9851cb08.png";
  const [likesMap, setLikesMap] = useState({});

 useEffect(() => {
   const fetchLikes = async () => {
     try {
       const promises = blogs.map(async (blog) => {
         const response = await axios.get(
           `${API_URL}/api/articles/${blog.id}/likeCount`
         );
         return { id: blog.id, likes: response.data.likes_count };
       });
       const likes = await Promise.all(promises);
       const likedMap = {};
       likes.forEach((like) => {
         if (like.likes !== undefined) { // Ajout d'une vérification pour s'assurer que les likes sont définis
           likedMap[like.id] = like.likes;
         }
       });
       setLikesMap(likedMap);
     } catch (error) {
       console.error(error);
        errorMessage(setShowToast, "You have already liked this article");
     }
   };
   fetchLikes();
 }, [blogs]);


  const handleLike = async (blogId) => {
    if (likedBlogs.includes(blogId)) {
      // Si le blog est déjà aimé, le retirer de la liste des likes
      setLikedBlogs((prevLikedBlogs) =>
        prevLikedBlogs.filter((id) => id !== blogId)
      );
      try {
        if (token) {
          const apiUrl = `${API_URL}/api/articles/${blogId}/unlike`;
          const config = {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          };
          await axios.delete(apiUrl, config);
          setLikesMap((prevLikesMap) => ({
            ...prevLikesMap,
            [blogId]: prevLikesMap[blogId] - 1,
          }));
        }
      } catch (error) {
        console.error(error);
      }
    } else {
      // Sinon, ajouter le blog à la liste des likes
      setLikedBlogs((prevLikedBlogs) => [...prevLikedBlogs, blogId]);
      try {
        if (token) {
          const apiUrl = `${API_URL}/api/articles/${blogId}/like`;
          const config = {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          };
          await axios.post(apiUrl, "", config);
          setLikesMap((prevLikesMap) => ({
            ...prevLikesMap,
            [blogId]: prevLikesMap[blogId] ? prevLikesMap[blogId] + 1 : 1,
          }));
        }
      } catch (error) {
        console.error(error);
      }
    }
  };

  const isBlogLiked = (blogId) => likedBlogs.includes(blogId);

  return (
    <div className="w-full py-[50px]">
      <div className="max-w-[1240px] mx-auto">
        <div className="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-2 ss:grid-cols-1 gap-8 px-4 text-black">
          {blogs.map((blog) => (
            <div
              key={blog.id}
              className="bg-white rounded-xl overflow-hidden drop-shadow-md transform hover:scale-105 transition-transform duration-300 flex flex-col justify-between"
            >
              <Link to={`/blog/${blog.id}`}>
                <img
                  className="h-56 w-full object-cover"
                  src={blog.article_image || defaultImage}
                  alt={blog.title}
                />
                <div className="p-8 pb-0">
                  <h3 className="font-bold text-2xl my-1">{blog.titre}</h3>
                  <p className="text-gray-600 text-xl">{blog.desc}</p>
                </div>
              </Link>
              <div className="flex justify-between px-8 p-3">
                <button
                  disabled={!token}
                  className={
                    `bg-white text-slate-500 px-4 py-2 rounded-md mt-3 hover:bg-gray-100` +
                    (isBlogLiked(blog.id)
                      ? "text-red-600 border-red-500 hover:text-red-600 "
                      : "text-blue-600 border-blue-500 hover:text-blue-600 ")
                  }
                  onClick={() => handleLike(blog.id)}
                >
                  {isBlogLiked(blog.id)
                    ? `${likesMap[blog.id]} ❤️`
                    : `${likesMap[blog.id]} 💙`}
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Blogs;