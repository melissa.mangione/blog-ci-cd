import React, { useState } from "react";
import { formatDate } from "../../utils";

const Comment = ({
  comment,
  onAddReply,
  currentUser,
  onEditComment,
  onDeleteComment,
  onEditReply,
  onDeleteReply,
}) => {
  const [newReply, setNewReply] = useState("");
  const [isEditing, setIsEditing] = useState(false);
  const [editedComment, setEditedComment] = useState(comment.text);

  const handleAddReply = () => {
    console.log(`Adding reply to comment ${comment.id}:`, newReply);
    setNewReply("");
    onAddReply(comment.id, newReply);
  };

  const handleEditComment = () => {
    setIsEditing(false);
    onEditComment(comment.id, editedComment);
    setEditedComment("");
  };

  const handleDeleteComment = () => {
    onDeleteComment(comment.id);
  };

  const handleEditReply = (replyId, editedReply) => {
    onEditReply(comment.id, replyId, editedReply);
  };

  const handleDeleteReply = (replyId) => {
    onDeleteReply(comment.id, replyId);
  };

  return (
    <div className="flex flex-col gap-2 p-3 bg-white rounded" key={comment.id}>
      <div className="flex flex-row gap-2">
        <img
          className="w-10 h-10 rounded-full object-cover"
          src={"https://placekitten.com/100/100"}
          alt={`Avatar of ${comment.user.username}`}
        />
        <div className="flex flex-col w-full">
          <h1 className="font-bold text-gray-900">{comment.user.username}</h1>
          <p className="text-gray-900">{comment.message}</p>
          <p className="text-right text-sm text-gray-500">
            {formatDate(comment.created_at)}
          </p>
        </div>
      </div>

      {/* {comment.replies && comment.replies.length > 0 && (
        <div className="ml-8 mt-2">
          {comment.replies.map((reply) => (
            <div className="flex flex-row gap-2" key={reply.id}>
              <img
                className="w-10 h-10 rounded-full object-cover"
                src={reply.avatar}
                alt={`Avatar of ${reply.name}`}
              />
              <div className="bg-gray-100 p-2 w-full rounded">
                <p
                  className={`font-bold ${
                    reply.name === comment.name
                      ? "text-blue-500"
                      : "text-gray-900"
                  }`}
                >
                  {reply.name}
                </p>
                <p className="text-gray-900">{reply.text}</p>
                <p className=" text-right text-sm text-gray-500">
                  {reply.date}
                </p>

                {isUserAdmin || reply.isOwner ? (
                  <div className="mt-2">
                    <button
                      onClick={() =>
                        handleEditReply(
                          reply.id,
                          prompt("Edit reply:", reply.text)
                        )
                      }
                      className="text-blue-500 mr-2"
                    >
                      Edit Reply
                    </button>
                    <button
                      onClick={() => handleDeleteReply(reply.id)}
                      className="text-red-500"
                    >
                      Delete Reply
                    </button>
                  </div>
                ) : null}
              </div>
            </div>
          ))}
        </div>
      )} */}

      {/* Section for adding/editing/deleting replies to a comment */}
      <div className="ml-8 mt-2">
        {isEditing ? (
          <div>
            <textarea
              className="border p-2 rounded w-full focus:outline-none focus:border-blue-500"
              value={editedComment}
              onChange={(e) => setEditedComment(e.target.value)}
            ></textarea>
            <button
              onClick={handleEditComment}
              className="p-2 ml-2 mt-3 float-right"
            >
              Save Edit ✔
            </button>
          </div>
        ) : (
          <>
           
            <button
              onClick={handleAddReply}
              className="p-2 ml-2 mt-3 float-right"
            >
              Add Reply ⤴
            </button>
          </>
        )}

        {currentUser &&
          (currentUser.roles === '["ROLE_ADMIN"]' ||
            currentUser.id === comment.user.id) && (
            <div className="mt-2">
              <button
                onClick={() => setIsEditing(!isEditing)}
                className="text-blue-500 mr-2"
              >
                {isEditing ? "Cancel Edit" : "Edit Comment"}
              </button>
              <button onClick={handleDeleteComment} className="text-red-500">
                Delete Comment
              </button>
            </div>
          )}
      </div>
    </div>
  );
};

const BlogComment = ({
  comments,
  currentUser,
  idArticle,
  handleCreateOrModifyOrDelete,
}) => {
  const [commentsToShow, setCommentstoShow] = useState(comments);
  const [newComment, setNewComment] = useState("");

  const handleAddComment = async () => {
    console.log("Adding new comment:", newComment);
    const res = await handleCreateOrModifyOrDelete({
      method: "post",
      table: "commentaires",
      data: {
        message: newComment,
        user: currentUser,
        article: idArticle,
      },
    });
    if (res) {
      setCommentstoShow((prev) => [...prev, res.data.data]);
      setNewComment("");
    }
  };

  const handleAddReply = (commentId, replyText) => {
    console.log(`Adding reply to comment ${commentId}:`, replyText);
  };

  const handleEditComment = async (commentId, editedComment) => {
    console.log(`Editing comment ${commentId}:`, editedComment);
    const res = await handleCreateOrModifyOrDelete({
      method: "patch",
      table: "commentaires",
      data: {
        message: editedComment,
        user: currentUser,
        article: idArticle,
      },
      id: commentId,
    });
    if (res) {
      setCommentstoShow((prev) => {
        const index = prev.findIndex((comment) => comment.id === commentId);

        if (index !== -1) {
          // Créer un nouveau tableau en remplaçant l'élément modifié
          const updatedComments = [...prev];
          updatedComments[index] = res.data.data;

          return updatedComments;
        }

        return prev;
      });
    }
  };

  const handleDeleteComment = async (commentId) => {
    console.log(`Deleting comment ${commentId}`);
    const res = await handleCreateOrModifyOrDelete({
      method: "delete",
      table: "commentaires",
      id: commentId,
    });
    if (res) {
      setCommentstoShow(comments.filter((c) => c.id !== commentId));
    }
  };

  const handleEditReply = (commentId, replyId, editedReply) => {
    console.log(
      `Editing reply ${replyId} in comment ${commentId}:`,
      editedReply
    );
  };

  const handleDeleteReply = (commentId, replyId) => {
    console.log(`Deleting reply ${replyId} in comment ${commentId}`);
  };

  return (
    <div className="py-10">
      <h2 className="text-2xl font-bold">Comment ({commentsToShow.length})</h2>
      <div className="flex flex-col gap-4 py-5">
        {commentsToShow.map((comment) => (
          <Comment
            key={comment.id}
            comment={comment}
            onAddReply={handleAddReply}
            onEditComment={handleEditComment}
            onDeleteComment={handleDeleteComment}
            onEditReply={handleEditReply}
            onDeleteReply={handleDeleteReply}
            currentUser={currentUser}
          />
        ))}
      </div>

      {/* Section for adding new comments */}

      {currentUser && (
        <div className="mt-4">
          <textarea
            className="border p-2 rounded w-full focus:outline-none focus:border-blue-500"
            placeholder="Add your comment..."
            value={newComment}
            onChange={(e) => setNewComment(e.target.value)}
          ></textarea>
          <button
            onClick={handleAddComment}
            className="p-2 ml-2 mt-3 float-right animate-bounce"
          >
            Add Comment ⤴
          </button>
        </div>
      )}
    </div>
  );
};

export default BlogComment;
