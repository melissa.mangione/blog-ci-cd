import React from "react";
import { useParams } from "react-router-dom";
import { Navbar } from "../index.js";
import BlogComment from "./BlogComment.jsx";
import useFetch from "../../hooks/useFetch.js";

const BlogContent = ({ blogs, currentUser, handleCreateOrModifyOrDelete }) => {
  const { id } = useParams();

  const blog = blogs.filter((blog) => blog.id == id);
  const defaultImage =
    "https://mir-s3-cdn-cf.behance.net/project_modules/fs/876c22100707927.5f0ec9851cb08.png";

  let { data, loading } = useFetch(`articles/${id}/commentaires`);

  if (loading) return <>Loading ...</>;

  return (
    <div>
      <div className="w-full pb-10 bg-[#f9f9f9]">
        <div className="max-w-[1240px] mx-auto">
          <div
            className="grid lg:grid-cols-3 md:grid-cols-3 sm:grid-cols-1 ss:grid-cols-1
                   md:gap-x-8 sm:gap-y-8 ss:gap-y-8 px-4 sm:pt-20 md:mt-0 ss:pt-20 text-black"
          >
            <div className="col-span-2">
              <img
                className="h-56 w-full object-cover rounded-lg shadow-md"
                src={blog[0].article_image || defaultImage}
              />
              <h1 className="font-bold text-2xl my-1 pt-5">{blog[0].titre}</h1>
              <div className="pt-5">
                <p>{blog[0].contenu}</p>
              </div>
            </div>

            <div className="my-4 md:my-0">
              <div className="items-center w-full bg-white rounded-xl drop-shadow-md p-5 h-[225px]">
                <div>
                  <img
                    className="p-2 w-32 h-32 rounded-full mx-auto object-cover"
                    src="../src/assets/img/Fichier 3.png"
                  />
                  <h3 className="font-bold text-2xl text-center text-gray-900 pt-3">
                    {blog[0].user.username}
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={"max-w-[1240px] mx-auto px-4"}>
          <BlogComment
            comments={data?.data}
            currentUser={currentUser}
            idArticle={id}
            handleCreateOrModifyOrDelete={handleCreateOrModifyOrDelete}
          />
        </div>
      </div>
    </div>
  );
};

export default BlogContent;
