import React, { useEffect, useState } from "react";
import { menu, close, logo } from "../assets";
import { Link } from "react-router-dom";
import { API_URL } from "../App";
import axios from "axios";

const Navbar = ({ currentUser }) => {
  const [toggle, setToggle] = useState(false);

  const disconnect = () => {
    localStorage.removeItem("token");
    location.reload();
  };

  return (
    <div className="w-full h-[80px] z-10 bg-white fixed drop-shadow-lg relative">
      <div className="flex justify-between items-center w-full h-full md:max-w-[1240px] m-auto">
        <div className="flex items-center">
          <Link to={"/"}>
            <img
              src={logo}
              alt="logo"
              className="sm:ml-10 ss:ml-10 md:ml-3 opacity-[55%] w-full h-[40px]"
            />
          </Link>
        </div>

        <div className="flex items-center">
          <ul className="hidden md:flex">
            <li>
              <Link to={"/"}>Home</Link>
            </li>
            <li>
              <Link to={"/about"}>About</Link>
            </li>
            <li>
              <Link to={"/blog"}>Blog</Link>
            </li>
            {currentUser && (
              <li>
                <Link to={"/profil"}>Profil</Link>
              </li>
            )}
            {currentUser?.roles === '["ROLE_ADMIN"]' && (
              <li>
                <Link to={"/back-office"}>Back Office</Link>
              </li>
            )}
          </ul>
        </div>

        <div className="hidden md:flex sm:mr-10 md:mr-10">
          {!currentUser ? (
            <>
              <Link to={"/login"}>
                <button className="border-none bg-transparent text-black mr-4 py-3">
                  Login
                </button>
              </Link>
              <Link to={"/signup"}>
                <button className="px-8 py-3">Sign Up</button>
              </Link>
            </>
          ) : (
            <button onClick={() => disconnect()} className="px-8 py-3">
              Se déconnecter
            </button>
          )}
        </div>

        <div className="md:hidden" onClick={() => setToggle(!toggle)}>
          <img
            src={!toggle ? menu : close}
            alt="menu"
            className="w-[28px] h-[28px] object-contain mr-10"
          />
        </div>
      </div>
      <ul
        className={
          toggle ? "absolute bg-white w-full px-8 md:hidden" : "hidden"
        }
      >
        <li>
          <Link to={"/"}>Home</Link>
        </li>
        <li>
          <Link to={"/about"}>About</Link>
        </li>
        <li>
          <Link to={"/blog"}>Blog</Link>
        </li>
        {currentUser && (
          <li>
            <Link to={"/profil"}>Profil</Link>
          </li>
        )}
        {currentUser?.roles === '["ROLE_ADMIN"]' && (
          <li>
            <Link to={"/back-office"}>Back Office</Link>
          </li>
        )}

        <div className="flex flex-col my-4">
          {!currentUser ? (
            <>
              <Link to={"/login"}>
                <button className="bg-transparent text-black mb-4 py-3 px-8 w-full">
                  Login
                </button>
              </Link>
              <Link to={"/signup"}>
                <button className="px-8 py-3 w-full">Sign Up</button>
              </Link>{" "}
            </>
          ) : (
            <button onClick={() => disconnect()} className="px-8 py-3 w-full">
              Se déconnecter
            </button>
          )}
        </div>
      </ul>
    </div>
  );
};

export default Navbar;
