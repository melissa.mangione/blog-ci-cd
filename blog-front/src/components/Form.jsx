import React from "react";

const Form = ({ action, title, button, children, validate = false }) => {
  return (
    <div className="bg-gray-200 flex flex-col items-center justify-center my-20 mx-auto p-10 w-full sm:w-2/3 md:w-1/2 lg:w-1/3 rounded">
      <h2 className="text-4xl mb-6">{title}</h2>
      <form className="flex flex-col gap-4 w-full">{children}</form>
      <button
        disabled={!validate}
        className={`btn p-2 mt-5 w-full ${!validate && "bg-transparent "}`}
        onClick={validate ? action : () => {}}
      >
        {button}
      </button>
    </div>
  );
};

export default Form;
