import axios from "axios";
import { useEffect, useState } from "react";
import { API_URL } from "../App";

const useFetch = (url) => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const token = localStorage.getItem("token");

  const fetchData = async () => {
    setLoading(true);
    try {
      const res = await axios.get(
        API_URL + "/api/" + url,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );

      setData(res.data);
      setLoading(false);
    } catch (error) {
      setError(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, [url]);

  const refetch = () => {
    fetchData();
  };

  return { loading, error, data, refetch };
};

export default useFetch;
