import { useEffect, useState } from "react";
import useFetch from "./hooks/useFetch";
import { errorMessage } from "./components/Toast";
import axios from "axios";
import { API_URL } from "./App";

export const TABLES = ["articles", "users", "categories", "commentaires"];

//Mettre la premiere lettre en majuscule
export const capitalizeFirstLetter = (str) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

//enlever le pluriel d'un mot
export const removeLastS = (str) => {
  if (str.charAt(str.length - 1) === "s") {
    return str.slice(0, -1);
  }
  return str;
};

//créer un objet correspondant aux champs de la table concernée
export const createObjectForTable = (table) => {
  const [structureObj, setStructureObj] = useState({});
  const token = localStorage.getItem("token");
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  let {
    loading,
    data: dataChampsTable,
    error,
  } = useFetch(`${table}Champs`, config);

  useEffect(() => {
    if (dataChampsTable) {
      const myObj = Object.keys(dataChampsTable).reduce((acc, label) => {
        acc[label] = "";
        return acc;
      }, {});
      setStructureObj(myObj);
    }
  }, [dataChampsTable]);

  return { loading, structureObj, error };
};

//Choisis le type de l'input en fonction de son label ( pas ouf a ameliorer )
export const typeInput = (label) => {
  if (label.includes("image")) return "file";
  if (label == "contenu" || label === "message") return "textarea";
  if (label.includes("password")) return "password";
  if (label === "category" || label === "user" || label === "article")
    return "number";
  return "text";
};

// Toutes les validations des champs
export const validateFormDatas = (table, dataObj, validateFunction) => {
  let validate;
  switch (table) {
    case "articles":
      validate = [
        dataObj.titre !== "",
        dataObj.contenu !== "",
        dataObj.article_image !== "",
        dataObj.category !== "",
      ];
      break;

    case "users":
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      validate = [
        dataObj.email.match(emailRegex),
        dataObj.password.length > 6,
        dataObj.username !== "",
      ];
      break;
    case "categories":
      validate = [dataObj.category_name.length > 3];
      break;

    case "commentaires":
      validate = [dataObj.message.length > 5];
      break;

    default:
      validate = [];
      break;
  }

  validateFunction(validate.every((v) => v));
};

export const formatDate = (date) => {
  const newDate = new Date(date);
  let day = newDate.getDate();
  let month = newDate.getMonth() + 1; // Les mois commencent à 0
  let year = newDate.getFullYear();

  // Ajout de zéros pour assurer le format "dd/mm/YYYY"
  day = day < 10 ? "0" + day : day;
  month = month < 10 ? "0" + month : month;

  return day + "/" + month + "/" + year;
};
