import React from "react";
import { render, screen, act } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Toast, { errorMessage } from "../src/components/Toast.jsx";

describe("Toast Component", () => {

    it("hides the toast after 5 seconds", async () => {
        jest.useFakeTimers();

        const setShowToast = jest.fn();

        render(<Toast />);

        act(() => {
            errorMessage(setShowToast);
        });

        // Advance time by 5 seconds
        jest.advanceTimersByTime(5000);

        expect(setShowToast).toHaveBeenCalledWith(true);
        expect(setShowToast).toHaveBeenCalledWith(false);
    });

});
