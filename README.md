# Projet de Blog avec Laravel, API REST et Docker

Ce projet est un blog complet développé en utilisant des technologies modernes, démontrant notre capacité à travailler avec des outils de développement, de containerisation, d'intégration continue, ainsi que notre compétence en matière de tests et de documentation.

## Technologies utilisées

- **Laravel:** Le backend du projet est développé avec le framework PHP Laravel, offrant une structure MVC robuste et des fonctionnalités avancées.
Laravel est un framework web moderne et populaire qui simplifie le développement d'applications en fournissant des fonctionnalités et des outils puissants comme la migration de base de données , le système de routage..  Laravel intègre aussi des fonctionnalités de sécurité telles que la protection CSRF (Cross-Site Request Forgery), la validation des données .

- **API REST:** Le projet expose une API REST pour la gestion des articles, des utilisateurs, des commentaires, etc.

- **Authentification par JWT Token:** L'authentification est assurée par des jetons JWT (JSON Web Tokens), garantissant un accès sécurisé aux fonctionnalités protégées.

- **Docker:** Le projet est dockerisé, permettant un déploiement facile et une gestion efficace des dépendances.

- **React:** Le projet à un coté client utilisant le framework ReactJs
 React encourage la création de composants réutilisables, ce qui peut considérablement simplifier le développement et la maintenance  du code. 
 React facilite l'intégration avec ces services à l'aide de bibliothèques telles que Axios
- **Tailwind**
L'utilisation de Tailwind CSS avec React est une combinaison populaire dans le développement web. Tailwind est conçu pour être facilement personnalisable. 


## Prérequis

Avant de commencer, assurez-vous d'avoir installé les outils suivants sur votre machine de développement :

- [Docker](https://www.docker.com/get-started)
- [Composer](https://getcomposer.org/download/)
- [npm](https://www.npmjs.com/)

## Installation

1. Clonez le repository :

    ```bash
    git clone https://gitlab.com/melissa.mangione/blog-ci-cd.git
    ```

2. Accédez au répertoire du projet :

    ```bash
    cd blog-back 
    
    ```

3. Copiez le fichier d'environnement :

    ```bash
    cp .env.example .env
    ```

4. Configurez votre fichier `.env` avec les informations appropriées (base de données, JWT secret, etc.).

5. Installez les dépendances PHP avec Composer :

    ```bash
    composer install
    ```

6. Générez les clés d'application Laravel et des dépendances :

    ```bash
    php artisan key:generate
    php artisan jwt:secret 
    ```

7. Exécutez les migrations et les seeders pour créer la base de données et les données initiales :

    ```bash
    php artisan migrate 
    ```

Pour le frontend

1. Accédez au répertoire du projet :

    ```bash
    cd blog-front 
    ```
2. Installez les dépendances :
    ```bash
    npm install 
    ```
    ```
3. Lancez le serveur
    ```bash
    npm run dev 
    ```
3. Builder l'application (prod)
    ```bash
    npm run build 
    ```
## Pour utiliser Docker

Pour démarrer l'application, utilisez Docker Compose :
Dans un terminal 
```bash
cd blog-back
docker-compose build
docker run -it blog_back bash
php artisan migrate

```
Pour le front dans un autre terminal

```bash
cd blog-back
docker-compose build
docker run -it blog_back bash

```
